"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatedJWT = void 0;
const config_1 = __importDefault(require("../config/config"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const validatedJWT = (token) => {
    try {
        const uid = jsonwebtoken_1.default.verify(token, config_1.default.SECRETJWT);
        return [true, uid.id];
    }
    catch (error) {
        return [false, null];
    }
};
exports.validatedJWT = validatedJWT;
