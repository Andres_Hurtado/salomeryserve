"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isBelong = exports.isUserAdmin = exports.verifyToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config/config"));
const user_1 = __importDefault(require("../models/auth/user"));
const stableciment_1 = __importDefault(require("../models/stableciment/stableciment"));
const roles_1 = __importDefault(require("../models/auth/roles"));
const product_1 = __importDefault(require("../models/product/product"));
const verifyToken = async (req, res, next) => {
    try {
        const token = req.headers["x-access-token"];
        if (!token)
            return res.status(403).json({ message: "No token provided" });
        const decoded = jsonwebtoken_1.default.verify(token, config_1.default.SECRETJWT);
        req.body.userId = decoded.id;
        const user = await user_1.default.findById(req.body.userId);
        if (!user)
            return res.status(404).json({ message: "no user found" });
        next();
    }
    catch (error) {
        return res.status(401).json({ message: "Unauthorized" });
    }
};
exports.verifyToken = verifyToken;
const isUserAdmin = async (req, res, next) => {
    const user = await user_1.default.findById(req.body.userId);
    const roles = await roles_1.default.find({ _id: { $in: user?.roles } });
    for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "user-admin") {
            next();
            return;
        }
    }
    return res.status(403).json({ message: "Require user-admin roles" });
};
exports.isUserAdmin = isUserAdmin;
/* SI EL PRODUCTO LE PERTENECE A ESE USUARIO */
const isBelong = async (req, res, next) => {
    const stableciment = await stableciment_1.default.findOne({ idUser: { $in: req.body.userId } });
    const product = await product_1.default.find({ stablecimentId: { $in: stableciment?._id } });
    if (product.length > 0) {
        next();
        return;
    }
    return res.status(403).json({ message: "Product is not property" });
};
exports.isBelong = isBelong;
