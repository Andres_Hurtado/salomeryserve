"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStablecimentById = exports.updateStablecimentById = exports.getStablecimentUserById = exports.getStablecimentById = exports.getStableciment = exports.createStableciment = void 0;
const stableciment_1 = __importDefault(require("../models/stableciment/stableciment"));
const user_1 = __importDefault(require("../models/auth/user"));
const roles_1 = __importDefault(require("../models/auth/roles"));
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const createStableciment = async (req, res) => {
    const { name, description, idUser, phoneNumber, ubication, state, image } = req.body;
    const newStableciment = new stableciment_1.default({
        name,
        description,
        idUser,
        phoneNumber,
        ubication,
        state,
        image,
    });
    const role = await roles_1.default.findOne({ name: "user-admin" });
    if (idUser) {
        const newUser = await user_1.default.findById(idUser);
        if (newUser) {
            newUser.roles = role;
            const updateUser = await user_1.default.findByIdAndUpdate(idUser, newUser, {
                new: true,
            });
        }
    }
    const staablecimentSaved = await newStableciment.save();
    return res.status(201).json(staablecimentSaved);
};
exports.createStableciment = createStableciment;
const getStableciment = async (req, res) => {
    const stableciment = await stableciment_1.default.find();
    return res.status(200).json(stableciment);
};
exports.getStableciment = getStableciment;
const getStablecimentById = async (req, res) => {
    const { stablecimentId } = req.params;
    const stableciment = await stableciment_1.default.findById(stablecimentId);
    if (!stableciment)
        return res
            .status(400)
            .json({ ok: false, message: "Stableciment not found" });
    return res.status(200).json(stableciment);
};
exports.getStablecimentById = getStablecimentById;
const getStablecimentUserById = async (req, res) => {
    console.log(req.body.userId);
    const { userId } = req.params;
    const stableciment = await stableciment_1.default.findOne({ idUser: userId });
    if (!stableciment)
        return res
            .status(400)
            .json({ ok: false, message: "Stableciment not found" });
    return res.status(200).json(stableciment);
};
exports.getStablecimentUserById = getStablecimentUserById;
const updateStablecimentById = async (req, res) => {
    const { stablecimentId } = req.params;
    const stableciment = req.body;
    stableciment.image = req.file?.path;
    const userPath = await stableciment_1.default.findById(stablecimentId);
    if (userPath?.image && req.file?.path && fs_extra_1.default.existsSync(userPath.image)) {
        await fs_extra_1.default.unlink(path_1.default.resolve(userPath.image));
    }
    const updateProduct = await stableciment_1.default.findByIdAndUpdate(stablecimentId, stableciment, {
        new: true,
    });
    return res.status(200).json(updateProduct);
};
exports.updateStablecimentById = updateStablecimentById;
const deleteStablecimentById = async (req, res) => {
    const { stablecimentId } = req.params;
    await stableciment_1.default.findByIdAndDelete(stablecimentId);
    return res.status(204).json();
};
exports.deleteStablecimentById = deleteStablecimentById;
