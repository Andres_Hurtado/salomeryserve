"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSizeProduct = exports.getCategorysProduct = exports.deleteProductById = exports.updateProductById = exports.getProductsStablecimentSaveOrDisabledById = exports.getProductsStablecimentById = exports.getProductsById = exports.getProducts = exports.createProduct = void 0;
const product_1 = __importDefault(require("../models/product/product"));
const statusProduct_1 = __importDefault(require("../models/product/statusProduct"));
const categoryProducts_1 = __importDefault(require("../models/product/categoryProducts"));
const sizeProducts_1 = __importDefault(require("../models/product/sizeProducts"));
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const stableciment_1 = __importDefault(require("../models/stableciment/stableciment"));
const createProduct = async (req, res) => {
    const { name, description, category, price, stablecimentId, state, size } = req.body;
    const newProduct = new product_1.default({
        name,
        description,
        price,
    });
    const files = req?.files;
    if (files.length > 0) {
        files.map((resp) => {
            newProduct.image.push(resp.path);
        });
    }
    const foundStableciment = await stableciment_1.default.findById(stablecimentId);
    if (!foundStableciment)
        return res
            .status(400)
            .json({ ok: false, message: "Stableciment not found" });
    newProduct.stablecimentId = foundStableciment?._id;
    if (category) {
        const foundCategopry = await categoryProducts_1.default.find({
            name: { $in: category },
        });
        newProduct.category = foundCategopry.map((category) => category._id);
    }
    if (size) {
        const foundSize = await sizeProducts_1.default.find({ name: { $in: size } });
        newProduct.size = foundSize.map((size) => size._id);
    }
    if (state) {
        const foundState = await statusProduct_1.default.find({ name: { $in: state } });
        newProduct.state = foundState.map((stat) => stat._id);
    }
    else {
        const stat = await statusProduct_1.default.findOne({ name: "save" });
        newProduct.state = [stat?._id];
    }
    const productSaved = await newProduct.save();
    return res.status(201).json(productSaved);
};
exports.createProduct = createProduct;
const getProducts = async (req, res) => {
    const from = Number(req.body.from) || 0;
    const products = await product_1.default.find()
        .populate("category")
        .populate("state")
        .populate("size")
        .skip(from)
        .limit(20);
    return res.status(200).json(products);
};
exports.getProducts = getProducts;
const getProductsById = async (req, res) => {
    const { productId } = req.params;
    const product = await product_1.default.findById(productId);
    return res.status(200).json(product);
};
exports.getProductsById = getProductsById;
const getProductsStablecimentById = async (req, res) => {
    const { stablecimentId } = req.params;
    const from = Number(req.body.from) || 0;
    const state = await statusProduct_1.default.findOne({ name: "published" });
    if (state) {
        const product = await product_1.default.find({
            stablecimentId: { $eq: stablecimentId },
            state: { $eq: state._id },
        })
            .populate("category")
            .populate("state")
            .populate("size")
            .skip(from)
            .limit(20);
        return res.status(200).json(product);
    }
    return res.status(400).json();
};
exports.getProductsStablecimentById = getProductsStablecimentById;
const getProductsStablecimentSaveOrDisabledById = async (req, res) => {
    const { stablecimentId } = req.params;
    const from = Number(req.body.from) || 0;
    const state = await statusProduct_1.default.find({
        $or: [{ name: "save" }, { name: "disabled" }],
    });
    if (state) {
        const id1 = state[0]._id;
        const id2 = state[1]._id;
        const product = await product_1.default.find({
            stablecimentId: { $eq: stablecimentId },
            $or: [{ state: id1 }, { state: id2 }],
        })
            .populate("category")
            .populate("state")
            .populate("size")
            .sort({ createdAt: 1 })
            .skip(from)
            .limit(20);
        return res.status(200).json(product);
    }
    return res.status(400).json();
};
exports.getProductsStablecimentSaveOrDisabledById = getProductsStablecimentSaveOrDisabledById;
const updateProductById = async (req, res) => {
    const { productId } = req.params;
    const product = req.body;
    const files = req?.files;
    if (files.length > 0) {
        files.map((resp) => {
            product.image.push(resp.path);
        });
    }
    const foundStableciment = await stableciment_1.default.findById(product.stablecimentId);
    if (!foundStableciment)
        return res
            .status(400)
            .json({ ok: false, message: "Stableciment not found" });
    product.stablecimentId = foundStableciment?._id;
    if (product.category) {
        const foundCategopry = await categoryProducts_1.default.find({
            name: { $in: product.category },
        });
        product.category = foundCategopry.map((category) => category._id);
    }
    if (product.size) {
        const foundSize = await sizeProducts_1.default.find({ name: { $in: product.size } });
        product.size = foundSize.map((size) => size._id);
    }
    if (product.state) {
        const foundState = await statusProduct_1.default.find({
            name: { $in: product.state },
        });
        product.state = foundState.map((stat) => stat._id);
    }
    const updateProduct = await product_1.default.findByIdAndUpdate(productId, product, {
        new: true,
    });
    console.log(product);
    return res.status(200).json(updateProduct);
};
exports.updateProductById = updateProductById;
const deleteProductById = async (req, res) => {
    const { productId } = req.params;
    const product = await product_1.default.findByIdAndDelete(productId);
    product?.image.map(async (resp) => {
        if (fs_extra_1.default.existsSync(resp)) {
            await fs_extra_1.default.unlink(path_1.default.resolve(resp));
        }
    });
    // await fs.rmdir('uploads/products/undefined', { recursive: true });
    return res.status(204).json();
};
exports.deleteProductById = deleteProductById;
const getCategorysProduct = async (req, res) => {
    const categoryProducts = await categoryProducts_1.default.find().sort({ name: 1 });
    return res.status(200).json(categoryProducts);
};
exports.getCategorysProduct = getCategorysProduct;
const getSizeProduct = async (req, res) => {
    const sizeProducts = await sizeProducts_1.default.find().sort({ name: 1 });
    return res.status(200).json(sizeProducts);
};
exports.getSizeProduct = getSizeProduct;
