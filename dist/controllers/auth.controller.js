"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renewToken = exports.deleteUserById = exports.updateUserById = exports.signIn = exports.signUp = void 0;
const user_1 = __importDefault(require("../models/auth/user"));
const stableciment_1 = __importDefault(require("../models/stableciment/stableciment"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config/config"));
const roles_1 = __importDefault(require("../models/auth/roles"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const path_1 = __importDefault(require("path"));
function createToken(user) {
    return jsonwebtoken_1.default.sign({ id: user.id }, config_1.default.SECRETJWT, {
        expiresIn: 86400, // 24 horas
    });
}
const signUp = async (req, res) => {
    const { firstName, lastName, phoneNumber, birthDate, address, image, roles } = req.body;
    const newUser = new user_1.default({
        firstName,
        lastName,
        phoneNumber,
        birthDate,
        address,
        image,
    });
    if (roles) {
        const foundRoles = await roles_1.default.find({ name: { $in: roles } });
        const role = foundRoles.map((role) => role._id);
        if (role.length > 0) {
            newUser.roles = role.shift();
        }
    }
    else {
        const role = await roles_1.default.findOne({ name: "user" });
        newUser.roles = role?.id;
    }
    const saveUser = await newUser.save();
    const user = await saveUser.populate('roles').execPopulate();
    const token = createToken(saveUser);
    return res.status(200).json({ ok: true, user: user, token });
};
exports.signUp = signUp;
const signIn = async (req, res) => {
    const { phoneNumber } = req.body;
    const userFound = await user_1.default.findOne({ phoneNumber }).populate("roles");
    if (!userFound)
        return res.status(400).json({ ok: false, message: "User not found" });
    const stablecimentFound = await stableciment_1.default.findOne({ idUser: userFound._id });
    const token = createToken(userFound);
    if (stablecimentFound)
        return res.json({ ok: true, user: userFound, stableciment: stablecimentFound, token });
    return res.json({ ok: true, user: userFound, token });
};
exports.signIn = signIn;
const updateUserById = async (req, res) => {
    const { userId } = req.params;
    const user = req.body;
    user.image = req.file?.path;
    const userPath = await user_1.default.findById(userId);
    if (userPath?.image && req.file?.path && fs_extra_1.default.existsSync(userPath.image)) {
        await fs_extra_1.default.unlink(path_1.default.resolve(userPath.image));
    }
    const updateUser = await user_1.default.findByIdAndUpdate(userId, user, {
        new: true,
    }).populate("roles");
    return res.status(200).json({ ok: true, user: updateUser });
};
exports.updateUserById = updateUserById;
const deleteUserById = async (req, res) => {
    const { userId } = req.params;
    await user_1.default.findByIdAndDelete(userId);
    return res.status(204).json();
};
exports.deleteUserById = deleteUserById;
const renewToken = async (req, res) => {
    const userFound = await user_1.default.findById(req.body.userId).populate("roles");
    if (!userFound)
        return res.status(400).json({ ok: false, message: "User not found" });
    const stablecimentFound = await stableciment_1.default.findOne({ idUser: userFound._id });
    const token = createToken(userFound);
    if (stablecimentFound)
        return res.json({ ok: true, user: userFound, stableciment: stablecimentFound, token });
    return res.json({ ok: true, user: userFound, token });
};
exports.renewToken = renewToken;
