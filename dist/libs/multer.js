"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const uuid_1 = require("uuid");
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        let ruta = "";
        const { userId } = req.params;
        const { stablecimentId } = req.body;
        if (stablecimentId != undefined) {
            const carpeta = stablecimentId;
            ruta = `./uploads/products/${carpeta}`;
        }
        else if (userId != undefined) {
            const carpeta = userId;
            ruta = `./uploads/users/${carpeta}`;
        }
        else {
            return;
        }
        fs_extra_1.default.mkdirsSync(ruta);
        cb(null, ruta);
    },
    filename: (req, file, cb) => {
        const uuid = uuid_1.v4();
        cb(null, uuid + path_1.default.extname(file.originalname));
    },
});
// const storage = multer.diskStorage({
//   destination: "uploads",
//   filename: (req, file, cb) => {
//     const uuid = v4();
//     cb(null, uuid + path.extname(file.originalname));
//   },
// });
exports.default = multer_1.default({ storage });
