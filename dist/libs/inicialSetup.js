"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createSizeProduct = exports.createCategoryProduct = exports.createStateProduct = exports.createRoles = void 0;
const roles_1 = __importDefault(require("../models/auth/roles"));
const categoryProducts_1 = __importDefault(require("../models/product/categoryProducts"));
const statusProduct_1 = __importDefault(require("../models/product/statusProduct"));
const sizeProducts_1 = __importDefault(require("../models/product/sizeProducts"));
const createRoles = async () => {
    try {
        const count = await roles_1.default.estimatedDocumentCount();
        if (count > 0)
            return;
        const values = await Promise.all([
            new roles_1.default({
                name: "user",
            }).save(),
            new roles_1.default({
                name: "user-admin",
            }).save(),
            new roles_1.default({
                name: "user-delivery",
            }).save(),
        ]);
    }
    catch (error) {
        console.log(error);
    }
};
exports.createRoles = createRoles;
const createStateProduct = async () => {
    try {
        const count = await statusProduct_1.default.estimatedDocumentCount();
        if (count > 0)
            return;
        const values = await Promise.all([
            new statusProduct_1.default({
                name: "published",
            }).save(),
            new statusProduct_1.default({
                name: "save",
            }).save(),
            new statusProduct_1.default({
                name: "disabled",
            }).save(),
        ]);
    }
    catch (error) {
        console.log(error);
    }
};
exports.createStateProduct = createStateProduct;
const createCategoryProduct = async () => {
    try {
        const count = await categoryProducts_1.default.estimatedDocumentCount();
        if (count > 0)
            return;
        const values = await Promise.all([
            new categoryProducts_1.default({
                name: "Hamburguesa",
                image: "https://gastronomiaycia.republica.com/wp-content/uploads/2020/11/mcplant_1-680x451.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Pizza",
                image: "https://cdn.colombia.com/gastronomia/2011/08/25/pizza-margarita-3684.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Perro Caliente",
                image: "https://www.eltiempo.com/files/image_640_428/uploads/2020/04/24/5ea2ef7236ff8.jpeg",
            }).save(),
            new categoryProducts_1.default({
                name: "Salchipapa",
                image: "https://www.comedera.com/wp-content/uploads/2021/07/salchipapas.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Carne",
                image: "https://saboryestilo.com.mx/wp-content/uploads/2020/01/tips-para-hacer-la-mejor-carne-asada-1200x720.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Pollo",
                image: "https://www.hola.com/imagenes/cocina/recetas/20200130159403/pollo-asado-en-horno-de-lena/0-779-940/pollo-asado-m.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Jugo Natural",
                image: "https://dam.cocinafacil.com.mx/wp-content/uploads/2020/01/jugos-saludables.jpg",
            }).save(),
            new categoryProducts_1.default({
                name: "Gaseosa",
                image: "https://thefoodtech.com/wp-content/uploads/2020/06/bebidas-gaseosas1.jpg",
            }).save(),
        ]);
    }
    catch (error) {
        console.log(error);
    }
};
exports.createCategoryProduct = createCategoryProduct;
const createSizeProduct = async () => {
    try {
        const count = await sizeProducts_1.default.estimatedDocumentCount();
        if (count > 0)
            return;
        const values = await Promise.all([
            /* food */
            new sizeProducts_1.default({
                type: "food",
                name: "Pequeño",
                image: "https://image.freepik.com/foto-gratis/pizza-pequena-caja-carton_105718-4013.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "food",
                name: "Normal",
                image: "https://as01.epimg.net/epik/imagenes/2017/10/31/portada/1509469785_213048_1509471547_noticia_normal.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "food",
                name: "Grande",
                image: "https://i.ytimg.com/vi/vk0VzJf8rDg/maxresdefault.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "food",
                name: "Extra Grande",
                image: "https://i.ytimg.com/vi/vk0VzJf8rDg/maxresdefault.jpg",
            }).save(),
            /* juice */
            new sizeProducts_1.default({
                type: "juice",
                name: "1/2 Jarra",
                image: "https://image.freepik.com/foto-gratis/jarra-jugo-naranja-frutas-naranja-aisladas_80510-975.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "juice",
                name: "1 Jarra",
                image: "https://image.freepik.com/foto-gratis/jarra-jugo-naranja-frutas-naranja-aisladas_80510-975.jpg",
            }).save(),
            /* drink */
            new sizeProducts_1.default({
                type: "drink",
                name: "200 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "237 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "250 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "330 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "350 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "500 ml",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "1 L",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "1.5 L",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "2 L",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
            new sizeProducts_1.default({
                type: "drink",
                name: "2.2 L",
                image: "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
            }).save(),
        ]);
    }
    catch (error) {
        console.log(error);
    }
};
exports.createSizeProduct = createSizeProduct;
