"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const sizeProductsSchema = new mongoose_1.Schema({
    type: String,
    name: String,
    image: String,
}, {
    versionKey: false,
});
exports.default = mongoose_1.model("SizeProduct", sizeProductsSchema);
