"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const categorySchema = new mongoose_1.Schema({
    name: String,
    image: String,
}, {
    versionKey: false,
});
exports.default = mongoose_1.model("CategoryProduct", categorySchema);
