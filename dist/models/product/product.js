"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const productSchema = new mongoose_1.Schema({
    name: String,
    description: String,
    category: {
        ref: "CategoryProduct",
        type: mongoose_1.Schema.Types.ObjectId,
    },
    price: { type: Number, default: 0 },
    stablecimentId: {
        ref: "Stableciment",
        type: mongoose_1.Schema.Types.ObjectId,
    },
    image: [String],
    state: {
        ref: "StateProduct",
        type: mongoose_1.Schema.Types.ObjectId,
    },
    size: {
        ref: "SizeProduct",
        type: mongoose_1.Schema.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
});
exports.default = mongoose_1.model("Product", productSchema);
