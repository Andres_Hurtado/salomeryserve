"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATE = void 0;
const mongoose_1 = require("mongoose");
exports.STATE = ["published", "save"];
const stateSchema = new mongoose_1.Schema({
    name: String,
}, {
    versionKey: false,
});
exports.default = mongoose_1.model("StateProduct", stateSchema);
