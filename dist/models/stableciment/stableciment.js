"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const stablecimentSchema = new mongoose_1.Schema({
    name: String,
    description: String,
    idUser: String,
    image: String,
    phoneNumber: [Number],
    ubication: String,
    state: Boolean,
}, {
    timestamps: true,
    versionKey: false,
});
exports.default = mongoose_1.model("Stableciment", stablecimentSchema);
