"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const userSchema = new mongoose_1.Schema({
    firstName: {
        type: String,
    },
    lastName: {
        type: String,
    },
    birthDate: {
        type: String,
    },
    address: {
        type: String,
    },
    image: {
        type: String,
    },
    phoneNumber: {
        type: Number,
        unique: true,
        required: true,
    },
    roles: {
        ref: "Role",
        type: mongoose_1.Schema.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
});
exports.default = mongoose_1.model("User", userSchema);
