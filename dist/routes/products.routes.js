"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const productsCrtl = __importStar(require("../controllers/products.controller"));
const middlewares_1 = require("../middlewares");
const multer_1 = __importDefault(require("../libs/multer"));
const router = express_1.Router();
router.post("/", [middlewares_1.authJwt.verifyToken, middlewares_1.authJwt.isUserAdmin], multer_1.default.array('image', 2), productsCrtl.createProduct);
router.get("/all", productsCrtl.getProducts);
router.get("/:productId", productsCrtl.getProductsById);
router.get("/all/stableciment/published/:stablecimentId", productsCrtl.getProductsStablecimentById);
router.get("/all/stableciment/saveOrdisable/:stablecimentId", productsCrtl.getProductsStablecimentSaveOrDisabledById);
router.put("/:productId", [middlewares_1.authJwt.verifyToken, middlewares_1.authJwt.isUserAdmin, middlewares_1.authJwt.isBelong], multer_1.default.array('image', 2), productsCrtl.updateProductById);
router.delete("/:productId", [middlewares_1.authJwt.verifyToken, middlewares_1.authJwt.isUserAdmin, middlewares_1.authJwt.isBelong], productsCrtl.deleteProductById);
router.get("/categories/all", productsCrtl.getCategorysProduct);
router.get("/size/all", productsCrtl.getSizeProduct);
exports.default = router;
