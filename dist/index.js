"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const database_1 = require("./database/database");
const main = async () => {
    database_1.startConnection();
    // await app.listen(app.get("port"));
    // console.log("serve on port ", app.get("port"));
    await app_1.default.listen(3000, (err) => {
        if (err)
            throw new Error(err);
        console.log("Servidor corriendo en puerto!", 3000);
    });
};
main();
