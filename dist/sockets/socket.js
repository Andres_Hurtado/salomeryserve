"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validatedJWT_1 = require("../helpers/validatedJWT");
const { io } = require("../app");
//Mensajes de sockets
io.on("connection", (client) => {
    const token = client.handshake.headers["x-access-token"];
    const [valido, uid] = validatedJWT_1.validatedJWT(token);
    if (!valido) {
        return client.disconnect();
    }
    console.log("Cliente conectado");
    client.on("disconnect", () => {
        console.log("Cliente desconectado");
    });
});
