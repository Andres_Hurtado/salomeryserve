"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const path_1 = __importDefault(require("path"));
const cors_1 = __importDefault(require("cors"));
const inicialSetup_1 = require("./libs/inicialSetup");
// import  pkg  from '../package.json';
/* inizialicer */
const app = express_1.default();
const auth_routes_1 = __importDefault(require("./routes/auth.routes"));
const products_routes_1 = __importDefault(require("./routes/products.routes"));
const stableciment_routes_1 = __importDefault(require("./routes/stableciment.routes"));
inicialSetup_1.createRoles();
inicialSetup_1.createStateProduct();
inicialSetup_1.createCategoryProduct();
inicialSetup_1.createSizeProduct();
// app.set('pkg', pkg);
/* settings */
app.set("port", process.env.PORT || 3000);
/* middlewares */
app.use(morgan_1.default("dev"));
app.use(cors_1.default());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(express_1.default.json());
/* description */
app.get("/", (rep, res) => {
    res.json({
        name: "Salomery Api",
        author: "Andres Hurtado",
        description: "Api Rest Salomery",
        version: "1.0.0",
    });
});
/* routes */
app.use("/api/products", products_routes_1.default);
app.use("/api/auth", auth_routes_1.default);
app.use("/api/stableciment", stableciment_routes_1.default);
app.disable('etag');
/* this folder for app will by used to store public files */
app.use("/uploads", express_1.default.static(path_1.default.resolve("uploads")));
//Node server
const server = require("http").createServer(app);
module.exports.io = require("socket.io")(server);
require("./sockets/socket");
exports.default = server;
