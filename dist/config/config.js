"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    SECRETJWT: process.env.JWT_SECRET || '-dl-salomery-Api-',
    DB: {
        URI: process.env.MONGODB_URI || "mongodb://localhost/salomerydb",
        USER: process.env.MONGODB_USER,
        PASS: process.env.MONGODB_PASS
    }
};
