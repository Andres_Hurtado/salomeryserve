import { validatedJWT } from "../helpers/validatedJWT";
const { io } = require("../app");

//Mensajes de sockets
io.on("connection", (client: any) => {
  const token = client.handshake.headers["x-access-token"];
  const [valido, uid] = validatedJWT(token);
  if (!valido) {
    return client.disconnect();
  }
  console.log("Cliente conectado");

  client.on("disconnect", () => {
    console.log("Cliente desconectado");
  });
});
