import multer from "multer";
import { v4 } from "uuid";
import path from "path";
import fs from "fs-extra";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let ruta = "";
    const { userId } = req.params;
    const { stablecimentId } = req.body;

    if (stablecimentId !=  undefined ) {
      const carpeta = stablecimentId;
      ruta = `./uploads/products/${carpeta}`;
    }else if(userId != undefined){
      const carpeta = userId;
      ruta = `./uploads/users/${carpeta}`;
    }else{
      return;
    }

    fs.mkdirsSync(ruta);
    cb(null, ruta);
  },
  filename: (req, file, cb) => {
    const uuid = v4();
    cb(null, uuid + path.extname(file.originalname));
  },
});

// const storage = multer.diskStorage({
//   destination: "uploads",
//   filename: (req, file, cb) => {
//     const uuid = v4();
//     cb(null, uuid + path.extname(file.originalname));
//   },
// });

export default multer({ storage });
