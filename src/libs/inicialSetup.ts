import Role from "../models/auth/roles";
import CategoryProduct from "../models/product/categoryProducts";
import StateProduct from "../models/product/statusProduct";
import SizeProduct from "../models/product/sizeProducts";

export const createRoles = async () => {
  try {
    const count = await Role.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Role({
        name: "user",
      }).save(),
      new Role({
        name: "user-admin",
      }).save(),
      new Role({
        name: "user-delivery",
      }).save(),
    ]);
  } catch (error) {
    console.log(error);
  }
};

export const createStateProduct = async () => {
  try {
    const count = await StateProduct.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new StateProduct({
        name: "published",
      }).save(),
      new StateProduct({
        name: "save",
      }).save(),
      new StateProduct({
        name: "disabled",
      }).save(),
    ]);
  } catch (error) {
    console.log(error);
  }
};

export const createCategoryProduct = async () => {
  try {
    const count = await CategoryProduct.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new CategoryProduct({
        name: "Hamburguesa",
        image:
          "https://gastronomiaycia.republica.com/wp-content/uploads/2020/11/mcplant_1-680x451.jpg",
      }).save(),
      new CategoryProduct({
        name: "Pizza",
        image:
          "https://cdn.colombia.com/gastronomia/2011/08/25/pizza-margarita-3684.jpg",
      }).save(),
      new CategoryProduct({
        name: "Perro Caliente",
        image:
          "https://www.eltiempo.com/files/image_640_428/uploads/2020/04/24/5ea2ef7236ff8.jpeg",
      }).save(),
      new CategoryProduct({
        name: "Salchipapa",
        image:
          "https://www.comedera.com/wp-content/uploads/2021/07/salchipapas.jpg",
      }).save(),
      new CategoryProduct({
        name: "Carne",
        image:
          "https://saboryestilo.com.mx/wp-content/uploads/2020/01/tips-para-hacer-la-mejor-carne-asada-1200x720.jpg",
      }).save(),
      new CategoryProduct({
        name: "Pollo",
        image:
          "https://www.hola.com/imagenes/cocina/recetas/20200130159403/pollo-asado-en-horno-de-lena/0-779-940/pollo-asado-m.jpg",
      }).save(),
      new CategoryProduct({
        name: "Jugo Natural",
        image:
          "https://dam.cocinafacil.com.mx/wp-content/uploads/2020/01/jugos-saludables.jpg",
      }).save(),
      new CategoryProduct({
        name: "Gaseosa",
        image:
          "https://thefoodtech.com/wp-content/uploads/2020/06/bebidas-gaseosas1.jpg",
      }).save(),
    ]);
  } catch (error) {
    console.log(error);
  }
};

export const createSizeProduct = async () => {
  try {
    const count = await SizeProduct.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      /* food */
      new SizeProduct({
        type: "food",
        name: "Pequeño",
        image:
          "https://image.freepik.com/foto-gratis/pizza-pequena-caja-carton_105718-4013.jpg",
      }).save(),
      new SizeProduct({
        type: "food",
        name: "Normal",
        image:
          "https://as01.epimg.net/epik/imagenes/2017/10/31/portada/1509469785_213048_1509471547_noticia_normal.jpg",
      }).save(),
      new SizeProduct({
        type: "food",
        name: "Grande",
        image: "https://i.ytimg.com/vi/vk0VzJf8rDg/maxresdefault.jpg",
      }).save(),
      new SizeProduct({
        type: "food",
        name: "Extra Grande",
        image: "https://i.ytimg.com/vi/vk0VzJf8rDg/maxresdefault.jpg",
      }).save(),

      /* juice */
      new SizeProduct({
        type: "juice",
        name: "1/2 Jarra",
        image:
          "https://image.freepik.com/foto-gratis/jarra-jugo-naranja-frutas-naranja-aisladas_80510-975.jpg",
      }).save(),
      new SizeProduct({
        type: "juice",
        name: "1 Jarra",
        image:
          "https://image.freepik.com/foto-gratis/jarra-jugo-naranja-frutas-naranja-aisladas_80510-975.jpg",
      }).save(),

      /* drink */

      new SizeProduct({
        type: "drink",
        name: "200 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "237 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "250 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "330 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "350 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "500 ml",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "1 L",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "1.5 L",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "2 L",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
      new SizeProduct({
        type: "drink",
        name: "2.2 L",
        image:
          "https://www.cocacolaespana.es/content/dam/one/es/es/body/sostenibilidad/nuestros-productos/historia-envases-coca-cola/tamanos-2000.jpg",
      }).save(),
    ]);
  } catch (error) {
    console.log(error);
  }
};
