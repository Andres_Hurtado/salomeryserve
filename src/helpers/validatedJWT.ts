import config from "../config/config";
import jwt from "jsonwebtoken";
import { UserPayload } from "../models/jwt/jwt";

export const validatedJWT = (token: string) => {
  try {
    const uid = jwt.verify(token, config.SECRETJWT) as UserPayload;
    return [true, uid.id];
  } catch (error) {
    return [false, null];
  }
};
