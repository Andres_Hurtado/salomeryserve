import express from "express";
import morgan from "morgan";
import path from "path";
import cors from "cors";
import {
  createRoles,
  createStateProduct,
  createCategoryProduct,
  createSizeProduct,
} from "./libs/inicialSetup";
// import  pkg  from '../package.json';

/* inizialicer */
const app = express();

import authRoutes from "./routes/auth.routes";
import productsRoutes from "./routes/products.routes";
import stablecimentRoutes from "./routes/stableciment.routes";

createRoles();
createStateProduct();
createCategoryProduct();
createSizeProduct();
// app.set('pkg', pkg);

/* settings */
app.set("port", process.env.PORT || 3000);

/* middlewares */
app.use(morgan("dev"));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

/* description */
app.get("/", (rep, res) => {
  res.json({
    name: "Salomery Api",
    author: "Andres Hurtado",
    description: "Api Rest Salomery",
    version: "1.0.0",
  });
});

/* routes */
app.use("/api/products", productsRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/stableciment", stablecimentRoutes);
app.disable('etag');

/* this folder for app will by used to store public files */
app.use("/uploads", express.static(path.resolve("uploads")));

//Node server
const server = require("http").createServer(app);
module.exports.io = require("socket.io")(server);
require("./sockets/socket");

export default server;
