import mongoose, { ConnectOptions } from "mongoose";
import config from "../config/config";

const dbOptions: ConnectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
};

export const startConnection = async () => {
  mongoose.connect(config.DB.URI, dbOptions);
  console.log("db is connected");
};
