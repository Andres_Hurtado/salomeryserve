import server from "./app";
const { app } = require('./app');

import { startConnection } from "./database/database";

const main = async () => {
  startConnection();
  // await app.listen(app.get("port"));
  // console.log("serve on port ", app.get("port"));
  

  await server.listen(3000 , (err: any) => {
    if (err) throw new Error(err);
    console.log("Servidor corriendo en puerto!", 3000);
  });

};

main();
