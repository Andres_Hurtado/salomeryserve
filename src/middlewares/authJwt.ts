import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import config from "../config/config";
import User from "../models/auth/user";
import Stableciment from "../models/stableciment/stableciment";
import Role from "../models/auth/roles";
import Product from "../models/product/product";
import { UserPayload } from "../models/jwt/jwt";

export const verifyToken = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers["x-access-token"] as string;
    if (!token) return res.status(403).json({ message: "No token provided" });

    const decoded = jwt.verify(token, config.SECRETJWT) as UserPayload;
    req.body.userId = decoded.id;
    const user = await User.findById(req.body.userId );
    if (!user) return res.status(404).json({ message: "no user found" });
    next();
  } catch (error) {
    return res.status(401).json({ message: "Unauthorized" });
  }
};

export const isUserAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = await User.findById(req.body.userId );
  const roles = await Role.find({ _id: { $in: user?.roles } });
  for (let i = 0; i < roles.length; i++) {
    if (roles[i].name === "user-admin") {
      next();
      return;
    }
  }
  return res.status(403).json({ message: "Require user-admin roles" });
};

/* SI EL PRODUCTO LE PERTENECE A ESE USUARIO */
export const isBelong = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const stableciment = await Stableciment.findOne({ idUser: { $in: req.body.userId  } });
  const product = await Product.find({ stablecimentId: { $in: stableciment?._id } });

  if (product.length > 0) {
    next();
    return;
  }

  return res.status(403).json({ message: "Product is not property" });
};
