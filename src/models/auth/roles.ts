import { Schema, model, Document } from "mongoose";

const roleSchema = new Schema<IRole>(
  {
    name: String,
  },
  {
    versionKey: false,
  }
);

export interface IRole  extends Document {
  name: string;
}

export default model<IRole>("Role", roleSchema);
