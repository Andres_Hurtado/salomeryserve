import { Schema, model, Document } from "mongoose";
import { IRole } from "./roles";

const userSchema = new Schema<IUser>(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    birthDate: {
      type: String,
    },
    address: {
      type: String,
    },
    image: {
      type: String,
    },
    phoneNumber: {
      type: Number,
      unique: true,
      required: true,
    },

    roles: [
      {
        ref: "Role",
        type: Schema.Types.ObjectId,
      },
    ],
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  birthDate: string;
  address: string;
  image: string;
  phoneNumber: number;
  roles: IRole[];
}

export default model<IUser>("User", userSchema);
