import { Schema, model, Document } from "mongoose";

export const STATE = ["published", "save"];

const stateSchema = new Schema(
  {
    name: String,
  },
  {
    versionKey: false,
  }
);

interface Istate extends Document {
  name: string;
}

export default model<Istate>("StateProduct", stateSchema);
