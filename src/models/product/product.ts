import { Schema, model, Document } from "mongoose";

const productSchema = new Schema<Iuser>(
  {
    name: String,
    description: String,
    category: {
      ref: "CategoryProduct",
      type: Schema.Types.ObjectId,
    },
    price: { type: Number, default: 0 },
    stablecimentId: {
      ref: "Stableciment",
      type: Schema.Types.ObjectId,
    },
    image: [String],
    state: {
      ref: "StateProduct",
      type: Schema.Types.ObjectId,
    },
    size: {
      ref: "SizeProduct",
      type: Schema.Types.ObjectId,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

interface Iuser extends Document {
  name: string;
  description: string;
  category: string[];
  price: number;
  stablecimentId: string;
  image: string[];
  state: string[];
  size: string[];
}

export default model<Iuser>("Product", productSchema);
