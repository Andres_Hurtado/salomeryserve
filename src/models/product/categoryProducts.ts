import { Schema, model } from "mongoose";

const categorySchema = new Schema<Icategory>(
  {
    name: String,
    image: String,
  },
  {
    versionKey: false,
  }
);

interface Icategory extends Document {
  name: string;
  image: string;
}

export default model<Icategory>("CategoryProduct", categorySchema);
