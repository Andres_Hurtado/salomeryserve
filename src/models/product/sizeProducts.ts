import { Schema, model } from "mongoose";

const sizeProductsSchema = new Schema<ISizeProducts>(
  {
    type: String,
    name: String,
    image: String,
  },
  {
    versionKey: false,
  }
);

interface ISizeProducts extends Document {
  type: string;
  name: string;
  image: string;
}

export default model<ISizeProducts>("SizeProduct", sizeProductsSchema);
