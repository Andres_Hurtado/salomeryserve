import { Schema, model, Document } from "mongoose";

const stablecimentSchema = new Schema<IStableciment>(
  {
    name: String,
    description: String,
    idUser: String,
    image: String,
    phoneNumber: [Number],
    ubication: String,
    state: Boolean,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export interface IStableciment extends Document {
  name: string;
  description: string;
  idUser: string;
  phoneNumber: string[];
  ubication: string;
  image: string;
  state: boolean;
}

export default model<IStableciment>("Stableciment", stablecimentSchema);
