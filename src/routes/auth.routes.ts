import  { Router } from 'express';
import * as authCrtl from '../controllers/auth.controller';
import { authJwt } from '../middlewares';
import multer from '../libs/multer';
const router = Router();

router.post('/signup', authCrtl.signUp)
router.post('/signin', authCrtl.signIn)
router.put('/account/:userId', authJwt.verifyToken,  multer.single('image'), authCrtl.updateUserById)
router.delete('/account/:userId', authJwt.verifyToken, authCrtl.deleteUserById)
router.get('/renew', authJwt.verifyToken, authCrtl.renewToken)

export default router;