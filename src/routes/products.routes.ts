import { Router } from "express";
import * as productsCrtl from "../controllers/products.controller";
import { authJwt } from '../middlewares'
import multer from '../libs/multer';
const router = Router();

router.post("/", [ authJwt.verifyToken, authJwt.isUserAdmin], multer. array('image', 2), productsCrtl.createProduct);
router.get("/all", productsCrtl.getProducts);
router.get("/:productId", productsCrtl.getProductsById);
router.get("/all/stableciment/published/:stablecimentId", productsCrtl.getProductsStablecimentById);
router.get("/all/stableciment/saveOrdisable/:stablecimentId", productsCrtl.getProductsStablecimentSaveOrDisabledById);
router.put("/:productId", [ authJwt.verifyToken, authJwt.isUserAdmin, authJwt.isBelong ], multer. array('image', 2), productsCrtl.updateProductById);
router.delete("/:productId", [ authJwt.verifyToken, authJwt.isUserAdmin, authJwt.isBelong  ],  productsCrtl.deleteProductById);

router.get("/categories/all", productsCrtl.getCategorysProduct);
router.get("/size/all", productsCrtl.getSizeProduct);

export default router;
