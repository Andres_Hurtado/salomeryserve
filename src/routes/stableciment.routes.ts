import { Router } from "express";
import * as stablecimentCrtl from "../controllers/stableciment.controller";
import { authJwt } from '../middlewares'
import multer from '../libs/multer';
import { getProductsStablecimentById } from '../controllers/products.controller';
const router = Router();

router.post("/", [ authJwt.verifyToken], stablecimentCrtl.createStableciment);
router.get("/all", stablecimentCrtl.getStableciment);
router.get("/:stablecimentId",[ authJwt.verifyToken], stablecimentCrtl.getStablecimentById);
router.get("/mystableciment/:userId",[ authJwt.verifyToken], stablecimentCrtl.getStablecimentUserById);
router.put("/:stablecimentId", [ authJwt.verifyToken, authJwt.isUserAdmin], stablecimentCrtl.updateStablecimentById);
router.delete("/:stablecimentId", [ authJwt.verifyToken, authJwt.isUserAdmin], stablecimentCrtl.deleteStablecimentById);

export default router;
