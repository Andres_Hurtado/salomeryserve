import { Request, Response } from "express";
import User, { IUser } from "../models/auth/user";
import Stableciment, {
  IStableciment,
} from "../models/stableciment/stableciment";
import jwt from "jsonwebtoken";
import config from "../config/config";
import Role, { IRole } from "../models/auth/roles";
import fs from "fs-extra";
import path from "path";

function createToken(user: IUser): string {
  return jwt.sign({ id: user.id }, config.SECRETJWT, {
    expiresIn: 86400, // 24 horas
  });
}

export const signUp = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { firstName, lastName, phoneNumber, birthDate, address, image, roles } =
    req.body;

  const newUser = new User({
    firstName,
    lastName,
    phoneNumber,
    birthDate,
    address,
    image,
  });

  if (roles) {
    const foundRoles = await Role.find({ name: { $in: roles } });
    newUser.roles = foundRoles.map<IRole>((role) => role._id);
  } else {
    const role = await Role.findOne({ name: "user" });
    newUser.roles = [role?.id];
  }

  const saveUser = await newUser.save();
  const user = await saveUser.populate('roles').execPopulate()
  const token = createToken(saveUser);
  return res.status(200).json({ ok: true, user: user, token });
};

export const signIn = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { phoneNumber } = req.body;
  const userFound = await User.findOne({ phoneNumber }).populate("roles");
  if (!userFound) return res.status(400).json({ ok: false, message: "User not found" });
  const stablecimentFound = await Stableciment.findOne({ idUser: userFound._id });
  const token = createToken(userFound);

  if(stablecimentFound) return res.json({ ok: true, user: userFound, stableciment: stablecimentFound, token });
  return res.json({ ok: true, user: userFound,  token });
};

export const updateUserById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { userId } = req.params;
  const user = req.body;
  user.image = req.file?.path;

  const userPath = await User.findById(userId);
  if (userPath?.image && req.file?.path && fs.existsSync(userPath!.image)) {
    await fs.unlink(path.resolve(userPath!.image));
  }
  const updateUser = await User.findByIdAndUpdate(userId, user, {
    new: true,
  }).populate("roles");

  return res.status(200).json({ ok: true, user: updateUser });
};

export const deleteUserById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { userId } = req.params;
  await User.findByIdAndDelete(userId);
  return res.status(204).json();
};

export const renewToken = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const userFound = await User.findById(req.body.userId).populate("roles");

  if (!userFound) return res.status(400).json({ ok: false, message: "User not found" });
  const stablecimentFound = await Stableciment.findOne({ idUser: userFound._id });
  const token = createToken(userFound);

  if(stablecimentFound) return res.json({ ok: true, user: userFound, stableciment: stablecimentFound, token });
  return res.json({ ok: true, user: userFound,  token });

};
