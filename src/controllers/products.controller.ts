import { Request, Response } from "express";
import Product from "../models/product/product";
import StateProduct from "../models/product/statusProduct";
import CategoryProduct from "../models/product/categoryProducts";
import SizeProduct from "../models/product/sizeProducts";
import path from "path";
import fs from "fs-extra";
import Stableciment from "../models/stableciment/stableciment";

export const createProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { name, description, category, price, stablecimentId, state, size } =
    req.body;
  const newProduct = new Product({
    name,
    description,
    price,
  });

  const files = req?.files as Express.Multer.File[];
  if (files.length > 0) {
    files.map((resp) => {
      newProduct.image.push(resp.path);
    });
  }
  const foundStableciment = await Stableciment.findById(stablecimentId);
  if (!foundStableciment)
    return res
      .status(400)
      .json({ ok: false, message: "Stableciment not found" });
  newProduct.stablecimentId = foundStableciment?._id;

  if (category) {
    const foundCategopry = await CategoryProduct.find({
      name: { $in: category },
    });
    newProduct.category = foundCategopry.map((category) => category._id);
  }

  if (size) {
    const foundSize = await SizeProduct.find({ name: { $in: size } });
    newProduct.size = foundSize.map((size) => size._id);
  }

  if (state) {
    const foundState = await StateProduct.find({ name: { $in: state } });
    newProduct.state = foundState.map((stat) => stat._id);
  } else {
    const stat = await StateProduct.findOne({ name: "save" });
    newProduct.state = [stat?._id];
  }

  const productSaved = await newProduct.save();
  return res.status(201).json(productSaved);
};

export const getProducts = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const from = Number(req.body.from) || 0;
  const products = await Product.find()
    .populate("category")
    .populate("state")
    .populate("size")
    .skip(from)
    .limit(20);
  return res.status(200).json(products);
};

export const getProductsById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { productId } = req.params;
  const product = await Product.findById(productId);
  return res.status(200).json(product);
};

export const getProductsStablecimentById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { stablecimentId } = req.params;
  const from = Number(req.body.from) || 0;

  const state = await StateProduct.findOne({ name: "published" });

  if (state) {
    const product = await Product.find({
      stablecimentId: { $eq: stablecimentId },
      state: { $eq: state._id },
    })
      .populate("category")
      .populate("state")
      .populate("size")
      .skip(from)
      .limit(20);

    return res.status(200).json(product);
  }
  return res.status(400).json();
};

export const getProductsStablecimentSaveOrDisabledById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { stablecimentId } = req.params;
  const from = Number(req.body.from) || 0;

  const state = await StateProduct.find({
    $or: [{ name: "save" }, { name: "disabled" }],
  });

  if (state) {
    const id1 = state[0]._id;
    const id2 = state[1]._id;
    const product = await Product.find({
      stablecimentId: { $eq: stablecimentId },
      $or: [{ state: id1 }, { state: id2 }],
    })
      .populate("category")
      .populate("state")
      .populate("size")
      .sort({ createdAt: 1 })
      .skip(from)
      .limit(20);
    return res.status(200).json(product);
  }
  return res.status(400).json();
};

export const updateProductById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { productId } = req.params;
  const product = req.body;

  const files = req?.files as Express.Multer.File[];
  if (files.length > 0) {
    files.map((resp) => {
      product.image.push(resp.path);
    });
  }

  const foundStableciment = await Stableciment.findById(product.stablecimentId);
  if (!foundStableciment)
    return res
      .status(400)
      .json({ ok: false, message: "Stableciment not found" });
  product.stablecimentId = foundStableciment?._id;

  if (product.category) {
    const foundCategopry = await CategoryProduct.find({
      name: { $in: product.category },
    });
    product.category = foundCategopry.map((category) => category._id);
  }

  if (product.size) {
    const foundSize = await SizeProduct.find({ name: { $in: product.size } });
    product.size = foundSize.map((size) => size._id);
  }

  if (product.state) {
    const foundState = await StateProduct.find({
      name: { $in: product.state },
    });
    product.state = foundState.map((stat) => stat._id);
  }
  const updateProduct = await Product.findByIdAndUpdate(productId, product, {
    new: true,
  });

  console.log(product);
  return res.status(200).json(updateProduct);
};

export const deleteProductById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { productId } = req.params;
  const product = await Product.findByIdAndDelete(productId);
  product?.image.map(async (resp) => {
    if (fs.existsSync(resp)) {
      await fs.unlink(path.resolve(resp));
    }
  });

  // await fs.rmdir('uploads/products/undefined', { recursive: true });
  return res.status(204).json();
};

export const getCategorysProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const categoryProducts = await CategoryProduct.find().sort({ name: 1 });
  return res.status(200).json(categoryProducts);
};

export const getSizeProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const sizeProducts = await SizeProduct.find().sort({ name: 1 });
  return res.status(200).json(sizeProducts);
};
