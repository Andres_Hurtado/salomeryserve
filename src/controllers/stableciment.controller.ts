import { Request, Response } from "express";
import Stableciment, {
  IStableciment,
} from "../models/stableciment/stableciment";
import User, { IUser } from "../models/auth/user";
import Role, { IRole } from "../models/auth/roles";
import path from "path";
import fs from "fs-extra";

export const createStableciment = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { name, description, idUser, phoneNumber, ubication, state, image } =
    req.body;
  const newStableciment = new Stableciment({
    name,
    description,
    idUser,
    phoneNumber,
    ubication,
    state,
    image,
  }); 

  const role = await Role.findOne({ name: "user-admin" });
  if (idUser) {
    const newUser = await User.findById(idUser);
    if (newUser) {
      newUser.roles = [role!]!;
      const updateUser = await User.findByIdAndUpdate(idUser, newUser, {
        new: true,
      });
    }
  }

  const staablecimentSaved = await newStableciment.save();
  return res.status(201).json(staablecimentSaved);
};

export const getStableciment = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const stableciment = await Stableciment.find();
  return res.status(200).json(stableciment);
};

export const getStablecimentById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { stablecimentId } = req.params;
  const stableciment = await Stableciment.findById(stablecimentId);
  if (!stableciment)
    return res
      .status(400)
      .json({ ok: false, message: "Stableciment not found" });
  return res.status(200).json(stableciment);
};

export const getStablecimentUserById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  console.log(req.body.userId);
  const { userId } = req.params;
  const stableciment = await Stableciment.findOne({ idUser: userId });
  if (!stableciment)
    return res
      .status(400)
      .json({ ok: false, message: "Stableciment not found" });
  return res.status(200).json(stableciment);
};

export const updateStablecimentById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { stablecimentId } = req.params;
  const stableciment = req.body;
  stableciment.image = req.file?.path;

  const userPath = await Stableciment.findById(stablecimentId);
  if (userPath?.image && req.file?.path && fs.existsSync(userPath!.image)) {
    await fs.unlink(path.resolve(userPath!.image));
  }
  const updateProduct = await Stableciment.findByIdAndUpdate(
    stablecimentId,
    stableciment,
    {
      new: true,
    }
  );
  return res.status(200).json(updateProduct);
};

export const deleteStablecimentById = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const { stablecimentId } = req.params;
  await Stableciment.findByIdAndDelete(stablecimentId);
  return res.status(204).json();
};
